import React, {Component} from 'react';
import AddTaskForm from "../../components/AddTaskForm/AddTaskForm";
import Tasks from "../../components/Tasks/Tasks";
import './TodoApp.css';
import Spinner from "../../components/Spinner/Spinner";
import {getTask, createTask, currentTaskChange, removeTask, checkedTaskHandler} from "../../store/actions";
import {connect} from "react-redux";

class TodoApp extends Component {

  componentDidMount() {
    this.props.getTask();
  }

  render() {
    return (
      !this.props.loading ?
      <div className="todo-form">
        <h3>My to-do list:</h3>
        <AddTaskForm
            change={this.props.currentTaskChange}
            clicked={this.props.createTask}
            value={this.props.currentTask} />
        { Object.keys(this.props.tasks).map(taskId => {
            return <Tasks
              text={this.props.tasks[taskId].task}
              key={taskId}
              isChecked={this.props.tasks[taskId].checked}
              checked={() => this.props.checkedTaskHandler(taskId)}
              removed={() => this.props.removeTask(taskId)} />
          })}
      </div> : <Spinner/>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentTask: state.currentTask,
    tasks: state.tasks,
    loading: state.loading
  }
};

const mapDispatchToProps = dispatch => {
  return {
    currentTaskChange: (e) => dispatch(currentTaskChange(e.target.value)),
    createTask: () => dispatch(createTask()),
    getTask: () => dispatch(getTask()),
    removeTask: (id) => dispatch(removeTask(id)),
    checkedTaskHandler: (id) => dispatch(checkedTaskHandler(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoApp);