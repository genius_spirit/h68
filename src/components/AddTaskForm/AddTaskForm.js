import React from 'react';
import './AddTaskForm.css';
import {FormControl, InputLabel, Input, Button} from "material-ui";


const AddTaskForm = (props) => {
  return(
    <div className="taskForm">
      <FormControl>
        <InputLabel htmlFor="name-input">New task</InputLabel>
        <Input id="taskForm-input" value={props.value} onChange={props.change}/>
      </FormControl>
      <div className='btn-wrapper'>
        <Button variant="raised" size="small" className='btn' onClick={props.clicked}>Add</Button>
      </div>
    </div>
  )
};

export default AddTaskForm;

