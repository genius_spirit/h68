import {CHECKED, FETCH_TASK_ERROR, FETCH_TASK_REQUEST, FETCH_TASK_SUCCESS, INPUT_CHANGE, STOP_LOADING} from "./actions";

const initialState = {
  tasks: [],
  currentTask: '',
  loading: false,
  error: ''
};

const reducer = (state = initialState, action) => {

  switch (action.type) {
    case INPUT_CHANGE:
      return {...state, currentTask: action.value};
    case FETCH_TASK_REQUEST:
      return {...state, loading: true};
    case FETCH_TASK_ERROR:
      return {...state, error: action.error};
    case FETCH_TASK_SUCCESS:
      return {...state, tasks: action.data, loading: false};
    case STOP_LOADING:
      return {...state, loading: false};
    case CHECKED:
      const tasksCopy = {...state.tasks};
      const task = {...state.tasks[action.id]};
      task.checked = !task.checked;
      tasksCopy[action.id] = task;
      return {...state, tasks: tasksCopy};
    default:
      return state;
  }

};

export default reducer;